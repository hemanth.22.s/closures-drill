function cacheFunction(cb) {
    var cache={};

    return function(arg){
        if (cache[arg]){
            console.log(cache)
           return cache[arg];
        }
        else{
           cache[arg] = cb(arg);
           console.log(cache)
            return cache[arg];
        }
    }
}

module.exports = cacheFunction;

