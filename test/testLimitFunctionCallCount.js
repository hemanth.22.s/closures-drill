const limitFunctionCallCount = require("../limitFunctionCallCount");

function add(a, b) {
    return a + b
}

const limitedAddFn = limitFunctionCallCount(add, 2);
console.log(limitedAddFn(1, 2));
console.log(limitedAddFn(3, 4)); 
console.log(limitedAddFn(4, 5)); 


