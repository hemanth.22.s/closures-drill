const cacheFunction = require("../cacheFunction");

function returnCall(arg){
    return arg;
}

const letCall = cacheFunction (returnCall);

console.log(letCall("hello"));
console.log(letCall("Hemanth"));
console.log(letCall("hello"));